FROM tensorflow/tensorflow:latest-gpu-py3

RUN apt update 
RUN apt install --yes graphviz-dev graphviz 
RUN pip install numpy scipy scikit-learn pandas matplotlib seaborn h5py pydot graphviz pydotplus keras jupyter 

WORKDIR notebooks
EXPOSE 9000
EXPOSE 6006

ENTRYPOINT jupyter notebook --port 9000 --NotebookApp.token='' --ip=0.0.0.0 --allow-root