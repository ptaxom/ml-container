# Docker container for Machine Learning and Data Science

[![build status]( https://gitlab.com/ptaxom/ml-container/badges/master/pipeline.svg)](https://gitlab.com/ptaxom/ml-container/commits/master)


Container for Machine learning and Data Science based on Python 3

Based on: [tensorflow:latest-gpu-py3](https://www.tensorflow.org/install/docker)


Includes:
1. tensorflow-gpu
2. keras
3. scikit-learn
4. scipy
5. pandas
6. seaborn
7. h5
8. graphviz and pydot for model visualization

# Dependencies
To use GPU powered back-end install nvidia-docker2
```sh
sudo apt-get install -y nvidia-docker2 
```

# Run container
```sh
docker run -it --runtime=nvidia --rm -p 9000:9000 -p 6006:6006 registry.gitlab.com/ptaxom/ml-container:keras
```

If you want to save your notebooks in some folder(for example ~/my_awesome_notebooks) use
```sh
docker run --runtime=nvidia --name keras -dt -v ~/my_awesome_notebooks:/notebooks -p 9000:9000  -p 6006:6006 registry.gitlab.com/ptaxom/ml-container:keras
```

# Usage
After starting the container type to browser address bar **localhost:9000**

Tensorboard will be on **localhost:6006**

# Test
To make sure, that install was correct create notebook file and execute one of tests:

```python
    import tensorflow as tf
    tf.test.is_gpu_available()
```

```python
    from keras import backend as K
    K.tensorflow_backend._get_available_gpus()
```

```python
    from tensorflow.python.client import device_lib
    device_lib.list_local_devices()
```